function chronoforms_data_loadstate($, formObj){
    formObj.find(':input[data-load-state="disabled"]').prop("disabled", true);
    formObj.find('*[data-load-state="hidden"]').css("display", "none");
    formObj.find(':input[data-load-state="hidden_parent"]').each(function(){
	if($(this).closest(".gcore-subinput-container").length > 0){
	    $(this).closest(".gcore-subinput-container").css("display", "none");
	}else if($(this).closest(".gcore-form-row").length > 0){
	    $(this).closest(".gcore-form-row").css("display", "none");
	}
    });
}
