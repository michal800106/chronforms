function chronoforms_validation_signs($, formObj, required_icon){
    formObj.find(":input[class*=validate]").each(function(){
	if($(this).attr("class").indexOf("required") >= 0 || $(this).attr("class").indexOf("group") >= 0){
	    var required_parent = [];
	    if($(this).closest(".gcore-subinput-container").length > 0){
		var required_parent = $(this).closest(".gcore-subinput-container");
	    }else if($(this).closest(".gcore-form-row, .form-group").length > 0){
		var required_parent = $(this).closest(".gcore-form-row, .form-group");
	    }
	    if(required_parent.length > 0){
		var required_label = required_parent.find("label");
		if(required_label.length > 0 && !required_label.first().hasClass("required_label")){
		    required_label.first().addClass("required_label");
		    required_label.first().html(required_label.first().html() + " " + required_icon);
		}
	    }
	}
    });
}
