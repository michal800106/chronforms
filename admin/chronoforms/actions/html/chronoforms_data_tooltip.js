function chronoforms_data_tooltip($, formObj, tip_icon){
    formObj.find(":input").each(function(){
	if($(this).data("tooltip") && $(this).closest(".gcore-input, .gcore-input-wide").length > 0){
	    var tipped_parent = [];
	    if($(this).closest(".gcore-subinput-container").length > 0){
		var tipped_parent = $(this).closest(".gcore-subinput-container");
	    }else if($(this).closest(".gcore-form-row, .form-group").length > 0){
		var tipped_parent = $(this).closest(".gcore-form-row, .form-group");
	    }
	    if(tipped_parent.length > 0){
		var tipped_label = tipped_parent.find("label");
		if(tipped_label.length > 0 && !tipped_label.first().hasClass("tipped_label")){
		    tipped_label.first().addClass("tipped_label");
		    var $tip = $(tip_icon);
		    $tip.data("content", $(this).data("tooltip"));
		    tipped_label.first().append($tip);
		}
	    }
	}
    });
    formObj.find(".input-tooltip").gtooltip();
}
