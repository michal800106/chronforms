function chronoforms_gvalidation($, form_id){
    $("#"+form_id).gvalidate();
    $("#"+form_id).find(":input").on("invalid.gvalidation", function(){
	var field = $(this);
	if(field.is(":hidden")){
	    if(field.closest(".tab-pane").length > 0){
		var tab_id = field.closest(".tab-pane").attr("id");
		$('a[href="#'+tab_id+'"]').closest(".nav").gtabs("get").show($('a[href="#'+tab_id+'"]'));
	    }
	    if(field.closest(".panel-collapse").length > 0){
		var slider_id = field.closest(".panel-collapse").attr("id");
		$('a[href="#'+slider_id+'"]').closest(".panel-group").gsliders("get").show($('a[href="#'+slider_id+'"]'));
	    }
	}
	if(field.data("wysiwyg") == "1"){
	    field.data("gvalidation-target", field.parent());
	}
    });
    $("#"+form_id).on("success.gvalidation", function(e){
	if($("#"+form_id).data("gvalidate_success")){
	    var gvalidate_success = $("#"+form_id).data("gvalidate_success");
	    if(gvalidate_success in window){
		window[gvalidate_success](e, $("#"+form_id));
	    }
	}
    });
    $("#"+form_id).on("fail.gvalidation", function(e){
	if($("#"+form_id).data("gvalidate_fail")){
	    var gvalidate_fail = $("#"+form_id).data("gvalidate_fail");
	    if(gvalidate_fail in window){
		window[gvalidate_fail](e, $("#"+form_id));
	    }
	}
    });
}
